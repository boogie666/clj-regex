(ns cljs-regex.core
  (:refer-clojure :exclude [derive empty cat * + def]))

(defonce patterns (atom {}))

(defrecord Empty [])
(def empty (->Empty))

(defrecord Eps [])
(def eps (->Eps))

(defrecord Cat [left right])
(defn cat [& items]
  (reduce ->Cat items))

(defrecord Alt [this that])
(defn alt [& items]
  (reduce ->Alt items))

(defrecord Rep [lang])
(def rep ->Rep)

(defrecord Named [name])
(defn named [name]
  (when-not (qualified-keyword? name)
    (throw (ex-info "Names of patterns must be namespaced keywords" {:value name})))
  (->Named name))

(defn def [name pattern]
  (do
    (when-not (qualified-keyword? name)
      (throw (ex-info "Names of patterns must be namespaced keywords" {:value name})))
    (swap! patterns assoc name pattern)
    pattern))

(defmulti nullable? type)
(def m-nullable? (memoize nullable?))

(defmethod nullable? Empty [_] false)

(defmethod nullable? Named [{:keys [name]}]
  (m-nullable? (get @patterns name)))

(defmethod nullable? Eps [_] true)
(defmethod nullable? Rep [_] true)

(defmethod nullable? Alt [{:keys [this that]}]
  (or (m-nullable? this)
      (m-nullable? that)))

(defmethod nullable? Cat [{:keys [left right]}]
  (and (m-nullable? left)
       (m-nullable? right)))

(defmethod nullable? :default [_] false)


(defmulti derive (fn [L c] (type L)))
(def m-derive (memoize derive))

(defmethod derive Empty [_ _] empty)

(defmethod derive Eps [_ _] empty)

(defmethod derive Named [{:keys [name]} value]
  (m-derive (get @patterns name) value))

(defmethod derive Alt [{:keys [this that]} c]
  (alt (m-derive this c)
       (m-derive that c)))

(defmethod derive Rep [{:keys [lang] :as rep} c]
  (cat (m-derive lang c)
       rep))

(defmethod derive Cat [{:keys [left right]} c]
  (if (m-nullable? left)
    (alt (m-derive right c)
         (cat (m-derive left c)
              right))
    (cat (m-derive left c)
         right)))

(defmethod derive :default [value c]
  (if (= value c)
    eps
    empty))

(defn maybe [L]
  (alt eps L))

(cljs-regex.core/def ::shallow-recursion
  (cat (maybe (named ::shallow-recursion)) \1))

(cljs-regex.core/def ::tree
  (rep (cat \[
            (named ::tree)
            \1
            \])))


(defn match [L xs]
  (m-nullable? (reduce m-derive L xs)))


(time (match (named ::tree) "[[[[[[[[[[[[[[[[[[[1]1]1]1]1]1]1]1]1]1]1]1]1]1]1]1]1]1]1]"))
(time (match (named ::tree) "[[[[[[1]1]1]1]1]1]"))
(time (match (named ::tree) "[[[[[[[[[1]1]1]1]1]1]1]1]1]"))
(time (match (named ::tree) "[[[[[[[[[[[[1]1]1]1]1]1]1]1]1]1]1]1]"))


